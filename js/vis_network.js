


$(document).ready(function () {

  const HOSTNAME = "http://localhost:3000";
  const API_VERSION = "v1";

  const API_URL = HOSTNAME + "/api/" + API_VERSION;

  var Methods = {
    DIJKSTRA: 'dijkstra',
    TWO_PATHS: 'two_paths',
    ALL_PATHS: 'all_paths'
  };

  var CalculationTypes = {
    NETWORK: 'network',
    NODE_PAIR: 'node_pair'
  };

  var network;
  var options;
  var container;
  var numberOfNodes;
  var numberOfEdges;
  var data = startNetwork();
  var nodes = data.nodes;
  var edges = data.edges;
  var networkID = 0;
  var tempNetwork;

  // Get the modal
  var modal = document.getElementById('myModal');
  // Get the <span> element that closes the modal
  var span = document.getElementsByClassName("close")[0];
  // Get report results paragraph
  var reportResults = document.getElementById('reportResults');

  // When the user clicks on <span> (x), close the modal
  span.onclick = function() {
    modal.style.display = "none";
  };

  // When the user clicks anywhere outside of the modal, close it
  window.onclick = function(event) {
    if (event.target == modal) {
      modal.style.display = "none";
    }
  };


  function startNetwork() {
    // $('#loadersmall').hide();

    $('#startNode').hide();
    $('#endNode').hide();
    $('#selectNodes').hide();
    $('#selectPathsmanually').hide();
    $('#pastschemes').hide();
    $('#results').hide();

    numberOfNodes = 0;
    numberOfEdges = 0;

    nodes = new vis.DataSet([]);
    edges = new vis.DataSet([]);

    container = document.getElementById('grid');


    var data = {
      nodes: nodes,
      edges: edges
    };

    options = {
      nodes: {
        shape: 'circle',
      },
      edges: {
        color: 'red'
      },
      physics: {
        enabled: false
      },
      manipulation: {
        addNode: function (nodeData, callback) {
          numberOfNodes = numberOfNodes + 1;
          nodeData.id = 'v' + numberOfNodes;
          nodeData.label = 'Node ' + numberOfNodes;
          nodeData.mi = 1;
          nodeData.lambda = 1;
          callback(nodeData);
          for (iter in nodes._data) {
            var node = nodes._data[iter];
            if (nodeData.id == node.id) {
              document.getElementById('operationNode').innerHTML = "Edit Node";
              document.getElementById('node-id').value = node.id;
              document.getElementById('node-label').value = node.label;
              document.getElementById('node-mi').value = node.mi;
              document.getElementById('node-lambda').value = node.lambda;
              document.getElementById('saveButtonNode').onclick = saveDataNode.bind(this, node, callback);
              //document.getElementById('cancelButtonNode').onclick = cancelEditNode.bind(this, callback);
              document.getElementById('network-popUp-Node').style.display = 'block';
            }

          }

        },
        editNode: function (data, callback) {
          // filling in the popup DOM elements
          for (iter in nodes._data) {
            var node = nodes._data[iter];
            if (data.id == node.id) {
              document.getElementById('operationNode').innerHTML = "Edit Node";
              document.getElementById('node-id').value = node.id;
              document.getElementById('node-label').value = node.label;
              document.getElementById('node-mi').value = node.mi;
              document.getElementById('node-lambda').value = node.lambda;
              document.getElementById('saveButtonNode').onclick = saveDataNode.bind(this, node, callback);
              //document.getElementById('cancelButtonNode').onclick = cancelEditNode.bind(this, callback);
              document.getElementById('network-popUp-Node').style.display = 'block';
            }
          }
        },
        addEdge: function (edgeData, callback) {
          numberOfEdges = numberOfEdges + 1;
          edgeData.id = 'e' + numberOfEdges;
          edgeData.label = 'Edge ' + numberOfEdges;
          edgeData.mi = 1;
          edgeData.lambda = 1;
          edgeData.length = 1;
          callback(edgeData);
          for (iter in edges._data) {
            var edge = edges._data[iter];
            if (edgeData.id == edge.id) {
              document.getElementById('operationEdge').innerHTML = "Edit Edge";
              document.getElementById('edge-id').value = edge.id;
              document.getElementById('edge-label').value = edge.label;
              document.getElementById('edge-mi').value = edge.mi;
              document.getElementById('edge-lambda').value = edge.lambda;
              document.getElementById('edge-length').value = edge.length;
              document.getElementById('saveButtonEdge').onclick = saveDataEdge.bind(this, edge, callback);
              //document.getElementById('cancelButtonEdge').onclick = cancelEditEdge.bind(this, callback);
              document.getElementById('network-popUp-Edge').style.display = 'block';
            }
          }

        },
        editEdge: function (data, callback) {
          // filling in the popup DOM elements
          for (iter in edges._data) {
            var edge = edges._data[iter];
            if (data.id == edge.id) {
              document.getElementById('operationEdge').innerHTML = "Edit Edge";
              document.getElementById('edge-id').value = edge.id;
              document.getElementById('edge-label').value = edge.label;
              document.getElementById('edge-mi').value = edge.mi;
              document.getElementById('edge-lambda').value = edge.lambda;
              document.getElementById('edge-length').value = edge.length;
              document.getElementById('saveButtonEdge').onclick = saveDataEdge.bind(this, edge, callback);
              //document.getElementById('cancelButtonEdge').onclick = cancelEditEdge.bind(this, callback);
              document.getElementById('network-popUp-Edge').style.display = 'block';
            }
          }
        }
      }
    };

    network = new vis.Network(container, data, options);

    return data;
  }

  $('#calculateButton').click(function () {
    prepareJSON();
    $.ajax({
      type: "POST",
      async: false,
      dataType: "json",
      contentType: "application/json",
      url: API_URL + "/calculate",
      data: JSON.stringify(data, null, 2)
    }).done(function (data) {
      document.getElementById('reportResults').innerHTML = JSON.stringify(data, null, 2);
      modal.style.display = "block";
    });
  });

  $('#saveAsButton').click(function () {
    prepareJSON();

    document.getElementById('operationScheme').innerHTML = "New Scheme";
    document.getElementById('schemeName').value = "";

    var networkJSON = {
      elements: data
    };

    document.getElementById('saveButtonScheme').onclick = saveDataScheme.bind(this, networkJSON);
    document.getElementById('save-popUp').style.display = 'block';
  });

  $('#saveButton').click(function () {
    if (tempNetwork) {
      prepareJSON();
      document.getElementById('operationScheme').innerHTML = "Edit Scheme:";
      document.getElementById('schemeName').value = tempNetwork.name;

      var networkJSON = {
        elements: data
      };

      document.getElementById('saveButtonScheme').onclick = updateDataScheme.bind(this, networkJSON);
      document.getElementById('save-popUp').style.display = 'block';

    } else {
      $('#saveAsButton').click();
    }

  });

  $('#reset').click(function () {
    networkID = 0;
    reset();
  });

  function reset() {
    document.getElementById('time').value = 100;
    data = startNetwork();
    nodes = data.nodes;
    edges = data.edges;
    $('input:radio[name="pairOrAll"]').prop('checked', false);
    $('input:radio[name="method"]').prop('checked', false);
  }

  function saveDataNode(data, callback) {
    data.id = document.getElementById('node-id').value;
    data.label = document.getElementById('node-label').value;
    data.mi = document.getElementById('node-mi').value;
    data.lambda = document.getElementById('node-lambda').value;
    clearPopUpNode();
    callback(data);
  }

  function saveDataEdge(data, callback) {
    data.id = document.getElementById('edge-id').value;
    data.label = document.getElementById('edge-label').value;
    data.mi = document.getElementById('edge-mi').value;
    data.lambda = document.getElementById('edge-lambda').value;
    data.length = document.getElementById('edge-length').value;
    clearPopUpEdge();
    callback(data);
  }

  function saveDataScheme(networkJSON) {
    networkJSON.name = document.getElementById('schemeName').value;
    $.ajax({
      type: "POST",
      async: false,
      dataType: "json",
      contentType: "application/json",
      url: API_URL + "/schemas",
      data: JSON.stringify(networkJSON)
    }).done(function (data) {
      tempNetwork = {
        id: data.schema.id,
        name: data.schema.name
      };

      document.getElementById('scheme-name').innerHTML = tempNetwork.name + " (" + tempNetwork.id + ") ";
      window.alert("Scheme was saved.");
    });
    clearPopUpSave();
  }

  function updateDataScheme(networkJSON) {
    networkJSON.name = document.getElementById('schemeName').value;
    $.ajax({
      type: "PUT",
      async: false,
      dataType: "json",
      contentType: "application/json",
      url: API_URL + "/schemas/" + networkID,
      data: JSON.stringify(networkJSON)
    }).done(function (data) {
      tempNetwork.name = document.getElementById('schemeName').value;
      document.getElementById('scheme-name').innerHTML = tempNetwork.name + " (" + tempNetwork.id + ") ";
      window.alert("Scheme was updated.");
    });
    clearPopUpSave();
  }

  function clearPopUpNode() {
    document.getElementById('saveButtonNode').onclick = null;
    //document.getElementById('cancelButtonNode').onclick = null;
    document.getElementById('network-popUp-Node').style.display = 'none';
  }

  function clearPopUpSave() {
    document.getElementById('saveButtonScheme').onclick = null;
    //document.getElementById('cancelButtonNode').onclick = null;
    document.getElementById('save-popUp').style.display = 'none';
  }

  function clearPopUpEdge() {
    document.getElementById('saveButtonEdge').onclick = null;
    //document.getElementById('cancelButtonEdge').onclick = null;
    document.getElementById('network-popUp-Edge').style.display = 'none';
  }

  function cancelEditNode(callback) {
    clearPopUpNode();
  }

  function cancelEditEdge(callback) {
    clearPopUpEdge();
  }

  $('input:radio[name="method"]').change(function () {
    if ($(this).val() == 'two_paths') {
      $('#selectNodes').hide();
      $('#startNode').hide();
      $('#endNode').hide();
      $('#selectPathsmanually').show();
    } else {
      $('input:radio[name="pairOrAll"]').prop('checked', false);
      $('#startNode').hide();
      $('#endNode').hide();
      $('#selectNodes').show();
      $('#selectPathsmanually').hide();
    }

  });

  $('input:radio[name="pairOrAll"]').change(function () {
    if ($(this).val() == 'node_pair') {
      $('#startNode').show();
      $('#endNode').hide();
      if (nodes.length > 0) {
        $('.startNode').find('option').remove().end();
        var selectOption = $("<option></option>").attr("value", "").attr("selected", true).attr("disabled", true).attr("hidden", true);
        $(".startNode").append(selectOption);
        for (iter in nodes._data) {
          var node = nodes._data[iter];
          addStartNodeSelection(node);
        }
        $('.startNode').change(function () {
          $('#endNode').show();
          $('.destinationNode').find('option').remove().end();
          var selectOption = $("<option></option>").attr("value", "").attr("selected", true).attr("disabled", true).attr("hidden", true);
          $(".destinationNode").append(selectOption);
          var startNode = $(this).find("option:selected");
          for (iter in nodes._data) {
            var node = nodes._data[iter];
            if (node.id != startNode.val()) {
              addDestinationNodeSelection(node);
            }
          }
        });
      }

    }
    else if ($(this).val() == 'network') {
      $('#startNode').hide();
      $('#endNode').hide();
    }
  });

  function addStartNodeSelection(node) {
    var selectOption = $("<option></option>").text(node.label).attr("value", node.id);
    $(".startNode").append(selectOption);
  }

  function addDestinationNodeSelection(node) {
    var selectOption = $("<option></option>").text(node.label).attr("value", node.id);
    $(".destinationNode").append(selectOption);
  }

  function prepareJSON() {
    switch ($('input:radio[name="method"]:checked').val()) {
      case (Methods.DIJKSTRA || Methods.ALL_PATHS):
        switch ($('input:radio[name="pairOrAll"]:checked').val()) {
          case CalculationTypes.NETWORK:
            settings = {
              calculation_type: CalculationTypes.NETWORK,
              path_method: $('input:radio[name="method"]:checked').val(),
              startNode: 'null',
              destinationNode: 'null',
              time: $('#time').val(),
              manuallPaths: 'null'
            };
            break;
          case CalculationTypes.NODE_PAIR:
            settings = {
              calculation_type: CalculationTypes.NODE_PAIR,
              path_method: $('input:radio[name="method"]:checked').val(),
              startNode: $('.startNode').find(":selected").val(),
              destinationNode: $('.destinationNode').find(":selected").val(),
              time: $('#time').val(),
              manuallPaths: 'null'
            };
            break;
          default:
            return false;
        }
        break;
      case Methods.TWO_PATHS:
        path1 = $('#path1').val().trim().split(",");
        path2 = $('#path2').val().trim().split(",");
        settings = {
          calculation_type: CalculationTypes.NODE_PAIR,
          path_method: Methods.TWO_PATHS,
          startNode: 'null',
          destinationNode: 'null',
          time: $('#time').val(),
          manuallPaths: [{
            manualPath1: path1
          },
            {
              manualPath2: path2
            }
          ]
        };
        break;
      default:
        return false;
    }

    data.userInput = settings;
  }

  $('#getPreviousSchemes').click(function () {
    $.ajax({
      type: "GET",
      async: false,
      dataType: "json",
      url: API_URL + "/schemas",
    }).done(function (data) {
      $('#pastSchemes').show();
      $('.pastSchemes').find('option').remove().end();
      var selectOption = $("<option></option>").attr("value", "").attr("selected", true).attr("disabled", true).attr("hidden", true);
      $(".pastSchemes").append(selectOption);
      for (iter in data.schemas) {
        var scheme = data.schemas[iter];
        addSchemeSelection(scheme);
      }
    });

  });

  function addSchemeSelection(scheme) {
    var selectOption = $("<option></option>").text(scheme.name).attr("value", scheme.id);
    $(".pastSchemes").append(selectOption);
  }

  $('.pastSchemes').change(function () {
    networkID = $('.pastSchemes').val();

    tempNetwork = {
      id: $('.pastSchemes').val(),
      name: $('#pastSchemes option:selected').text()
    };

    document.getElementById('scheme-name').innerHTML = tempNetwork.name + " (" + tempNetwork.id + ") ";

    $('#pastSchemes').hide();
    reset();
    importNetwork();
  });

  function importNetwork() {
    var networkNodes = [];
    var networkEdges = [];
    $.ajax({
      type: "GET",
      async: false,
      dataType: "json",
      url: API_URL + "/schemas/" + networkID
    }).done(function (data) {
      networkNodes = [];
      for (iter in data.schema.elements.nodes._data) {
        var node = data.schema.elements.nodes._data[iter];
        numberOfNodes = numberOfNodes + 1;
        networkNodes.push({
          id: node.id,
          label: node.label,
          x: node.x,
          y: node.y,
          lambda: node.lambda,
          mi: node.mi
        });
      }

      networkEdges = [];
      for (iter in data.schema.elements.edges._data) {
        var edge = data.schema.elements.edges._data[iter];
        numberOfEdges = networkEdges + 1;
        networkEdges.push({
          id: edge.id,
          label: edge.label,
          from: edge.from,
          to: edge.to,
          lambda: edge.lambda,
          mi: edge.mi,
          length: edge.length
        });
      }
    });

    data = loadNetwork(networkNodes, networkEdges);
  }

  function loadNetwork(networkNodes, networkEdges) {

    $('#startNode').hide();
    $('#endNode').hide();
    $('#selectNodes').hide();
    $('#selectPathsmanually').hide();
    $('#pastschemes').hide();
    $('#results').hide();

    //numberOfNodes = 0;
    //numberOfEdges = 0;

// create an array with nodes
    nodes = new vis.DataSet(networkNodes);

// create an array with edges
    edges = new vis.DataSet(networkEdges);

// create a network
    container = document.getElementById('grid');

// provide the data in the vis format
    var data = {
      nodes: nodes,
      edges: edges
    };
    options = {
      nodes: {
        shape: 'circle'
      },
      edges: {
        color: 'red'
      },
      physics: {
        enabled: false
      },
      manipulation: {
        addNode: function (nodeData, callback) {
          numberOfNodes = numberOfNodes + 1;
          nodeData.id = 'v' + numberOfNodes;
          nodeData.label = 'Node ' + numberOfNodes;
          nodeData.mi = 1;
          nodeData.lambda = 1;
          callback(nodeData);
          for (iter in nodes._data) {
            var node = nodes._data[iter];
            if (nodeData.id == node.id) {
              document.getElementById('operationNode').innerHTML = "Edit Node";
              document.getElementById('node-id').value = node.id;
              document.getElementById('node-label').value = node.label;
              document.getElementById('node-mi').value = node.mi;
              document.getElementById('node-lambda').value = node.lambda;
              document.getElementById('saveButtonNode').onclick = saveDataNode.bind(this, node, callback);
              //document.getElementById('cancelButtonNode').onclick = cancelEditNode.bind(this, callback);
              document.getElementById('network-popUp-Node').style.display = 'block';
            }

          }

        },
        editNode: function (data, callback) {
          // filling in the popup DOM elements
          for (iter in nodes._data) {
            var node = nodes._data[iter];
            if (data.id == node.id) {
              document.getElementById('operationNode').innerHTML = "Edit Node";
              document.getElementById('node-id').value = node.id;
              document.getElementById('node-label').value = node.label;
              document.getElementById('node-mi').value = node.mi;
              document.getElementById('node-lambda').value = node.lambda;
              document.getElementById('saveButtonNode').onclick = saveDataNode.bind(this, node, callback);
              //document.getElementById('cancelButtonNode').onclick = cancelEditNode.bind(this, callback);
              document.getElementById('network-popUp-Node').style.display = 'block';
            }
          }
        },
        addEdge: function (edgeData, callback) {
          numberOfEdges = numberOfEdges + 1;
          edgeData.id = 'e' + numberOfEdges;
          edgeData.label = 'Edge ' + numberOfEdges;
          edgeData.mi = 1;
          edgeData.lambda = 1;
          edgeData.length = 1;
          callback(edgeData);
          for (iter in edges._data) {
            var edge = edges._data[iter];
            if (edgeData.id == edge.id) {
              document.getElementById('operationEdge').innerHTML = "Edit Edge";
              document.getElementById('edge-id').value = edge.id;
              document.getElementById('edge-label').value = edge.label;
              document.getElementById('edge-mi').value = edge.mi;
              document.getElementById('edge-lambda').value = edge.lambda;
              document.getElementById('edge-length').value = edge.length;
              document.getElementById('saveButtonEdge').onclick = saveDataEdge.bind(this, edge, callback);
              //document.getElementById('cancelButtonEdge').onclick = cancelEditEdge.bind(this, callback);
              document.getElementById('network-popUp-Edge').style.display = 'block';
            }
          }

        },
        editEdge: function (data, callback) {
          // filling in the popup DOM elements
          for (iter in edges._data) {
            var edge = edges._data[iter];
            if (data.id == edge.id) {
              document.getElementById('operationEdge').innerHTML = "Edit Edge";
              document.getElementById('edge-id').value = edge.id;
              document.getElementById('edge-label').value = edge.label;
              document.getElementById('edge-mi').value = edge.mi;
              document.getElementById('edge-lambda').value = edge.lambda;
              document.getElementById('edge-length').value = edge.length;
              document.getElementById('saveButtonEdge').onclick = saveDataEdge.bind(this, edge, callback);
              //document.getElementById('cancelButtonEdge').onclick = cancelEditEdge.bind(this, callback);
              document.getElementById('network-popUp-Edge').style.display = 'block';
            }
          }
        }
      }
    };
    network = new vis.Network(container, data, options);
    return data;
  }



});


