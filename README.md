# PTM Simulator Frontend #


U ovom repozitoriju nalazi se klijentski dio sustava simulatora.

### Postavljanje ###

* Klonirati repozitorij
* U datoteci `js/vis_network.js` treba postaviti točnu konfiguraciju backend dijela sustava, točnije promijeniti sljedeće parametre

```javascript
const HOSTNAME = "http://localhost:3000";
const API_VERSION = "v1";
```
Sada se aplikacija može pokrenuti otvaranjem `index.html` datoteke u pregledniku.

### Who do I talk to? ###

Maintainers:

* Karlo Krčelić (karlo.krcelic@fer.hr)
* Matija Pevec (matija.pevec@fer.hr)